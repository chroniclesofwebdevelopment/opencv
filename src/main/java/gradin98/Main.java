package gradin98;

import gradin98.algorithms.IAlgorithm;
import gradin98.algorithms.derivation.ScharrDerivation;
import gradin98.algorithms.derivation.SobelDerivation;
import gradin98.algorithms.filtering.BilateralFilter;
import gradin98.algorithms.filtering.ErosionFilter;
import gradin98.algorithms.filtering.Filter2DFilter;
import gradin98.algorithms.transformation.ColorMapTransformation;
import gradin98.algorithms.transformation.RotationTransformation;

public class Main {

    public static void main(String args[]){
        nu.pattern.OpenCV.loadShared();
        System.loadLibrary(org.opencv.core.Core.NATIVE_LIBRARY_NAME);

//        IAlgorithm blur = new AveragingBlur(10,10,7,7);
//        blur.setImage("D:/facultate/Prelucrarea Imaginilor/imagini/original.jpg");
//        blur.processImage();

//        IAlgorithm blur = new GaussianBlur(11,11,7,7);
//        gaussianBlur.setImage("D:/facultate/Prelucrarea Imaginilor/imagini/original.jpg");
//        gaussianBlur.processImage();

//        IAlgorithm medianBlur = new MedianBlur(11);
//        medianBlur.setImage("D:/facultate/Prelucrarea Imaginilor/imagini/original.jpg");
//        medianBlur.processImage();

//        IAlgorithm colorMapTransformation = new ColorMapTransformation(ColorMapTransformation.ColorMapType.OCEAN);
//        colorMapTransformation.setImage("D:/facultate/Prelucrarea Imaginilor/imagini/original.jpg");
//        colorMapTransformation.processImage();

//        IAlgorithm rotationTransformation = new RotationTransformation(400,400,1,180);
//        rotationTransformation.setImage("D:/facultate/Prelucrarea Imaginilor/imagini/original.jpg");
//        rotationTransformation.processImage();

//        IAlgorithm sobelDerivation = new SobelDerivation(SobelDerivation.SobelDerivationEnumTypes.SOBEL_0_1);
//        sobelDerivation.setImage("D:/facultate/Prelucrarea Imaginilor/imagini/original.jpg");
//        sobelDerivation.processImage();

//        IAlgorithm scharDerivation = new ScharrDerivation(ScharrDerivation.ScharDerivationEnumTypes.SCHARR_1_0);
//        scharDerivation.setImage("D:/facultate/Prelucrarea Imaginilor/imagini/original.jpg");
//        scharDerivation.processImage();

//        IAlgorithm bilateralFilter = new BilateralFilter(10,200,30);
//        bilateralFilter.setImage("D:/facultate/Prelucrarea Imaginilor/imagini/original.jpg");
//        bilateralFilter.processImage();

//        IAlgorithm erosionFilter = new ErosionFilter();
//        erosionFilter.setImage("D:/facultate/Prelucrarea Imaginilor/imagini/original.jpg");
//        erosionFilter.processImage();

//        IAlgorithm filter2DFilter = new Filter2DFilter();
//        filter2DFilter.setImage("D:/facultate/Prelucrarea Imaginilor/imagini/original.jpg");
//        filter2DFilter.processImage();


    }
}
