package gradin98.algorithms;

public abstract class IAlgorithm {

    protected String filePath;

    public void setImage(String filePath) {
        this.filePath = filePath;
    }

    public abstract void processImage();
}
