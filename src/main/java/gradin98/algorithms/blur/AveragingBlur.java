package gradin98.algorithms.blur;

import gradin98.algorithms.IAlgorithm;
import gradin98.helpers.FileHelper;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.core.Point;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.File;

/*
    MIN VALUE:
        - WIDTH: 1
        - HEIGHT: 1
        - X = 0
        - Y = 0

    MAX VALUE:
        - WIDTH: IMAGE_WIDTH
        - HEIGHT: IMAGE_HEIGHT
        - X: IMAGE_WIDTH - 1
        - Y: IMAGE_HEIGHT - 1
 */
public class AveragingBlur extends IAlgorithm {

    private final int width;
    private final int height;
    private final int x;
    private final int y;

    public AveragingBlur(int width, int height, int x, int y) {
        this.width = width;
        this.height = height;
        this.x = x;
        this.y = y;
    }

    public void processImage() {
        Mat src = Imgcodecs.imread(this.filePath);
        Mat dst = new Mat();

        Size size = new Size(this.width, this.height);
        Point point = new Point(this.x, this.y);

        Imgproc.blur(src, dst, size, point, Core.BORDER_DEFAULT);

        String exportPath = FileHelper.getProcessedImageName(this.filePath);
        Imgcodecs.imwrite(exportPath, dst);
    }


}
