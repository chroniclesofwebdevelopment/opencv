package gradin98.algorithms.transformation;

import gradin98.algorithms.IAlgorithm;
import gradin98.helpers.FileHelper;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

public class ColorMapTransformation extends IAlgorithm {

    private final ColorMapType type;

    public ColorMapTransformation(ColorMapType type) {
        this.type = type;
    }


    public void processImage() {
        Mat src = Imgcodecs.imread(this.filePath);
        Mat dst = new Mat();

        Imgproc.applyColorMap(src, dst, this.type.getTypeValue());

        String exportPath = FileHelper.getProcessedImageName(this.filePath);
        Imgcodecs.imwrite(exportPath, dst);
    }

    public enum ColorMapType{
        AUTUMN(Imgproc.COLORMAP_AUTUMN),
        BONE(Imgproc.COLORMAP_BONE),
        COOL(Imgproc.COLORMAP_COOL),
        HOT(Imgproc.COLORMAP_HOT),
        HSV(Imgproc.COLORMAP_HSV),
        JET(Imgproc.COLORMAP_JET),
        OCEAN(Imgproc.COLORMAP_OCEAN),
        PARULA(Imgproc.COLORMAP_PARULA),
        PINK(Imgproc.COLORMAP_PINK),
        RAIMBOW(Imgproc.COLORMAP_RAINBOW),
        SPRING(Imgproc.COLORMAP_SPRING),
        SUMMER(Imgproc.COLORMAP_SUMMER),
        WINTER(Imgproc.COLORMAP_WINTER);

        private int type;

        ColorMapType(int type){
            this.type = type;
        }

        int getTypeValue(){
            return this.type;
        }

    }
}
