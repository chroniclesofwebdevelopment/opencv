package gradin98.algorithms.transformation;

import gradin98.algorithms.IAlgorithm;
import gradin98.helpers.FileHelper;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

public class RotationTransformation extends IAlgorithm {

    private final int width;
    private final int height;
    private final int size;
    private final int rotation;

    public RotationTransformation(int width, int height, int size, int rotation) {
        this.width = width;
        this.height = height;
        this.size = size;
        this.rotation = rotation;
    }

    public void processImage() {
        Mat src = Imgcodecs.imread(this.filePath);
        Mat dst = new Mat();

        Point point = new Point(this.width, this.height);
        Mat rotationMatrix = Imgproc.getRotationMatrix2D(point, rotation, 1);
        Size size = new Size(src.cols(), src.cols());

        Imgproc.warpAffine(src, dst, rotationMatrix, size);

        String exportPath = FileHelper.getProcessedImageName(this.filePath);
        Imgcodecs.imwrite(exportPath, dst);
    }


}
