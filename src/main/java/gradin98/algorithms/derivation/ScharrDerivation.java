package gradin98.algorithms.derivation;

import gradin98.algorithms.IAlgorithm;
import gradin98.helpers.FileHelper;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

public class ScharrDerivation extends IAlgorithm {

    private final ScharDerivationEnumTypes type;

    public ScharrDerivation(ScharrDerivation.ScharDerivationEnumTypes type) {
        this.type = type;
    }

    public void processImage() {
        Mat src = Imgcodecs.imread(this.filePath);
        Mat dst = new Mat();

        Imgproc.Scharr(src, dst, Imgproc.CV_SCHARR, type.getX(), type.getY());

        String exportPath = FileHelper.getProcessedImageName(this.filePath);
        Imgcodecs.imwrite(exportPath, dst);
    }

    public enum ScharDerivationEnumTypes{

        SCHARR_0_1(0,1),
        SCHARR_1_0(1,0);

        private final int x;
        private final int y;

        ScharDerivationEnumTypes(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }
    }
}
