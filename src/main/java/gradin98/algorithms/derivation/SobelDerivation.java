package gradin98.algorithms.derivation;

import gradin98.algorithms.IAlgorithm;
import gradin98.helpers.FileHelper;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

public class SobelDerivation extends IAlgorithm {

    private final SobelDerivationEnumTypes type;

    public SobelDerivation(SobelDerivationEnumTypes type) {
        this.type = type;
    }

    public void processImage() {
        Mat src = Imgcodecs.imread(this.filePath);
        Mat dst = new Mat();

        Imgproc.Sobel(src, dst, -1, type.getX(), type.getY());

        String exportPath = FileHelper.getProcessedImageName(this.filePath);
        Imgcodecs.imwrite(exportPath, dst);
    }

    public enum SobelDerivationEnumTypes{

        SOBEL_0_1(0,1),
        SOBEL_1_0(1,0),
        SOBEL_1_1(1,1);

        private final int x;
        private final int y;

        SobelDerivationEnumTypes(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }
    }
}
