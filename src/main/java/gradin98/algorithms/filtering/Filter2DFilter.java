package gradin98.algorithms.filtering;

import gradin98.algorithms.IAlgorithm;
import gradin98.helpers.FileHelper;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

public class Filter2DFilter extends IAlgorithm {

    public void processImage() {
        Mat src = Imgcodecs.imread(this.filePath);
        Mat dst = new Mat();

        Mat kernel = Mat.ones(2,2, CvType.CV_32F);


        for(int i = 0; i<kernel.rows(); i++) {
            for(int j = 0; j<kernel.cols(); j++) {
                double[] m = kernel.get(i, j);

                for(int k = 1; k<m.length; k++) {
                    m[k] = m[k]/(2 * 2);
                }
                kernel.put(i,j, m);
            }
        }

        Imgproc.filter2D(src, dst,-1, kernel);

        String exportPath = FileHelper.getProcessedImageName(this.filePath);
        Imgcodecs.imwrite(exportPath, dst);
    }
}
