package gradin98.algorithms.filtering;

import gradin98.algorithms.IAlgorithm;
import gradin98.helpers.FileHelper;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

public class BilateralFilter extends IAlgorithm {

    private final int diameter;
    private final int sigmaColor;
    private final int sigmaSpace;

    public BilateralFilter(int diameter, int sigmaColor, int sigmaSpace) {
        this.diameter = diameter;
        this.sigmaColor = sigmaColor;
        this.sigmaSpace = sigmaSpace;
    }

    public void processImage() {
        Mat src = Imgcodecs.imread(this.filePath);
        Mat dst = new Mat();

        Imgproc.bilateralFilter(src, dst, diameter, sigmaColor, sigmaSpace, Core.BORDER_DEFAULT);

        String exportPath = FileHelper.getProcessedImageName(this.filePath);
        Imgcodecs.imwrite(exportPath, dst);
    }
}
