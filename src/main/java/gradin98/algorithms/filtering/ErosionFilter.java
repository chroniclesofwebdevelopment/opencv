package gradin98.algorithms.filtering;

import gradin98.algorithms.IAlgorithm;
import gradin98.helpers.FileHelper;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

public class ErosionFilter extends IAlgorithm {

    public void processImage() {
        Mat src = Imgcodecs.imread(this.filePath);
        Mat dst = new Mat();

        Mat kernel = Imgproc.getStructuringElement(Imgproc.MORPH_RECT,
                new Size((2*2) + 1, (2*2)+1));

        Imgproc.erode(src, dst, kernel);

        String exportPath = FileHelper.getProcessedImageName(this.filePath);
        Imgcodecs.imwrite(exportPath, dst);
    }
}
