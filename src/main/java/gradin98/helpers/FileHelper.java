package gradin98.helpers;

import java.io.File;

public class FileHelper {

    public static String getProcessedImageName(String path){
        File file = new File(path);
        String directory = file.getParent();
        String filename = path.substring(path.lastIndexOf("/"));
        String extension = filename.substring(filename.indexOf("."));

        return directory  +
                filename.substring(0,filename.lastIndexOf(".")) + "_Processed" + extension;
    }
}
